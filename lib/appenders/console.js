"use strict";
var layouts = require('../layouts')
  , logger = require('../logger')
  , consoleLog = console.log.bind(console )
  , previousData = void(0)
  , previousEvent = void(0)
  , repeatCount = 0;

function consoleAppender (layout) {
  layout = layout || layouts.colouredLayout;
  return function(loggingEvent) {

    if( previousData && loggingEvent.data.join() == previousData ) {
      repeatCount++;
    } else {
      if( 0 < repeatCount ) {
        var event = new logger.LoggingEvent(previousEvent.categoryName, previousEvent.level, ["Previous message repeated " + repeatCount + " times."], previousEvent.logger);
        consoleLog(layout(event));
      }

      consoleLog(layout(loggingEvent));

      previousData = loggingEvent.data.join();
      previousEvent = loggingEvent;
      repeatCount = 0;
    }
  };
}

function configure(config) {
  var layout;
  if (config.layout) {
    layout = layouts.layout(config.layout.type, config.layout);
  }
  return consoleAppender(layout);
}

exports.appender = consoleAppender;
exports.configure = configure;
